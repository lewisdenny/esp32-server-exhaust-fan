# esp32-server-exhust-fan

HA = Home Assistant


## Checklist

- [x] Send temp data to HA
- [x] send fan speed HA
- [x] send dutycycle to HA
- [x] Set fan speed from HA
- [ ] send external temp to HA
- [ ] automate fan speed with temp
- [ ] Add small local display
- [ ] Add 3d printed case
- [ ] add local control for fan speed
- [ ] Fix all the crappy code :D 
 
![image](./images/ha-office-exhaust.png)

## API Ref
- [MQTTPubSubClient](https://github.com/hideakitai/MQTTPubSubClient#apis)
- [Adafruit_EMC2101](https://adafruit.github.io/Adafruit_EMC2101/html/)

## Hardware
- [nf-a15-pwm](https://noctua.at/en/products/fan/nf-a15-pwm)
- [Adafruit EMC2101](https://www.adafruit.com/product/4808)
- [FireBeetle ESP32-E](https://www.dfrobot.com/product-2195.html)