//#include <WiFi.h> Uncomment this for firebeatle
#include <ESP8266WiFi.h> //remove this out for firebeatle
#include <string.h>
#include <PubSubClient.h>
#include <Adafruit_EMC2101.h>
#include "arduino_secrets.h"
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Fonts/FreeSerif9pt7b.h>


#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);


//Please enter your sensitive data in to arduino_secrets.h
const char* ssid = SECRET_SSID;    
const char* password = SECRET_PASS;    
const char* mqtt_server = MQTT_SERVER;

WiFiClient espClient;
PubSubClient client(espClient);
Adafruit_EMC2101  emc2101;

unsigned long lastMsg = 0;
unsigned long previousMillis = 0;
unsigned long interval = 30000;

#define MSG_BUFFER_SIZE	(50)
char msg[MSG_BUFFER_SIZE];
int value = 0;

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  char * state = "/esp32/office/exhaust-fan/state";
  char * dutycycle = "/esp32/office/exhaust-fan/dutycycle/set";
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  if(strcmp(topic, state)){
    if((char*)payload == "true"){
      client.publish("/esp32/office/exhaust-fan/state", "true");
      emc2101.setDutyCycle(100);
    }else{
      client.publish("/esp32/office/exhaust-fan/state", "false");
      emc2101.setDutyCycle(0);
    }
  }
  if(strcmp(topic, dutycycle)){

    payload[length] = '\0';
    int aNumber = atoi((char *)payload);

    Serial.print("Setting duty cycle to: ");
    Serial.println(aNumber);

    emc2101.setDutyCycle(aNumber);
  }
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "CONNECTED OR RECONNECTED");
      // ... and resubscribe
      client.subscribe("/esp32/office/exhaust-fan/set");
      client.subscribe("/esp32/office/exhaust-fan/dutycycle/set");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);


  while (!emc2101.begin()) {
    delay(100);
    Serial.println("Failed to find EMC2101 chip");
    emc2101.begin();
   }
   Serial.println("EMC2101 Found!");
   emc2101.enableTachInput(true);
   emc2101.setPWMDivisor(0);
   //emc2101.setDataRate(EMC2101_RATE_1_16_HZ);

   if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for(;;);
  }
}

void loop() {
  unsigned long currentMillis = millis();
  // if WiFi is down, try reconnecting every CHECK_WIFI_TIME seconds
  if ((WiFi.status() != WL_CONNECTED) && (currentMillis - previousMillis >=interval)) {
    Serial.print(millis());
    Serial.println("Reconnecting to WiFi...");
    WiFi.disconnect();
    WiFi.reconnect();
    previousMillis = currentMillis;
  }


  if (!client.connected()) {
    reconnect();
  }
  client.loop();


  // publish message
  char onboardTemp [50], dutyCycle [50], rpm [50];
  int a = sprintf (onboardTemp, "%d", emc2101.getInternalTemperature());
  int b = sprintf (dutyCycle, "%d", emc2101.getDutyCycle());
  int c = sprintf (rpm, "%d", emc2101.getFanRPM());

  unsigned long now = millis();
  if (now - lastMsg > 2000) {
    lastMsg = now;

    Serial.print("Publish message: Onboard Temperature: ");
    Serial.println(onboardTemp);
    client.publish("/esp32/office/exhaust-fan/onboardtemp", onboardTemp);

    Serial.print("Publish message: Duty Cycle: ");
    Serial.println(dutyCycle);
    client.publish("/esp32/office/exhaust-fan/dutycycle", dutyCycle);

    Serial.print("Publish message: Fan RPM: ");
    Serial.println(rpm);
    client.publish("/esp32/office/exhaust-fan/rpm", rpm);

    Serial.print("Publish message: Fan State: ");
    Serial.println("on");
    client.publish("/esp32/office/exhaust-fan/state", "on");



    display.clearDisplay();
    display.setFont(&FreeSerif9pt7b);
    display.setTextSize(1);
    display.setRotation(2);
    display.setTextColor(WHITE);
    display.setCursor(10, 35);
    // Display static text
    display.print("Temp:  ");
    display.print(onboardTemp);
    display.println("c");
    display.setCursor(10, 54);
    display.print("Speed: ");
    display.print(rpm);
    display.println("rpm");
    display.display(); 
  }
}